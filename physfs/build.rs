use std::env;
use std::path::Path;

fn main() {
    let manifest_dir = env::var_os("CARGO_MANIFEST_DIR").unwrap();
    let headers = Path::new(&manifest_dir).join("include");

    cc::Build::new()
        .file("src/physfs.c")
        .file("src/physfs_archiver_dir.c")
        .file("src/physfs_archiver_unpacked.c")
        .file("src/physfs_archiver_zip.c")
        .file("src/physfs_byteorder.c")
        .file("src/physfs_unicode.c")
        .file("src/physfs_platform_posix.c")
        .file("src/physfs_platform_unix.c")
        .file("src/physfs_platform_windows.c")
        .define("PHYSFS_SUPPORTS_DEFAULT", "0")
        .define("PHYSFS_SUPPORTS_ZIP", "1")
        .include("include")
        .flag_if_supported("-std=gnu99")
        .flag("-U_FORTIFY_SOURCE")
        .warnings(false)
        .compile("physfs");

    println!("cargo:include={}", headers.to_str().unwrap());
}
