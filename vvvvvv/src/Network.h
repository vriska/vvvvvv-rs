#ifndef VNETWORK_H
#define VNETWORK_H

#include <stdint.h>

int32_t NETWORK_init(void);

void NETWORK_shutdown(void);

void NETWORK_update(void);

void NETWORK_unlockAchievement(const char *name);

int32_t NETWORK_getAchievementProgress(const char *name);

void NETWORK_setAchievementProgress(const char *name, int32_t stat);

#endif /* VNETWORK_H */
