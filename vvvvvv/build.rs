const FILES: &[&str] = &[
    "src/BinaryBlob.cpp",
    "src/BlockV.cpp",
    "src/Ent.cpp",
    "src/Entity.cpp",
    "src/FileSystemUtils.cpp",
    "src/Finalclass.cpp",
    "src/Game.cpp",
    "src/Graphics.cpp",
    "src/GraphicsResources.cpp",
    "src/GraphicsUtil.cpp",
    "src/Input.cpp",
    "src/KeyPoll.cpp",
    "src/Labclass.cpp",
    "src/Logic.cpp",
    "src/Map.cpp",
    "src/Music.cpp",
    "src/Otherlevel.cpp",
    "src/preloader.cpp",
    "src/Render.cpp",
    "src/RenderFixed.cpp",
    "src/Screen.cpp",
    "src/Script.cpp",
    "src/Scripts.cpp",
    "src/SoundSystem.cpp",
    "src/Spacestation2.cpp",
    "src/TerminalScripts.cpp",
    "src/Textbox.cpp",
    "src/Tower.cpp",
    "src/UtilityClass.cpp",
    "src/WarpClass.cpp",
    "src/XMLUtils.cpp",
    "src/main.cpp",
    "src/DeferCallbacks.cpp",
    "src/GlitchrunnerMode.cpp",
    "src/Network.cpp",
    "src/ThirdPartyDeps.cpp",
    "src/Vlogging.cpp",
    "src/physfsrwops.cpp",
    "src/CustomLevels.cpp",
    "src/Editor.cpp",
];

const HEADERS: &[&str] = &[
    "src/BinaryBlob.h",
    "src/BlockV.h",
    "src/Constants.h",
    "src/Credits.h",
    "src/CustomLevels.h",
    "src/DeferCallbacks.h",
    "src/Editor.h",
    "src/Ent.h",
    "src/Entity.h",
    "src/Enums.h",
    "src/Exit.h",
    "src/FileSystemUtils.h",
    "src/Finalclass.h",
    "src/Game.h",
    "src/GlitchrunnerMode.h",
    "src/Graphics.h",
    "src/GraphicsResources.h",
    "src/GraphicsUtil.h",
    "src/Input.h",
    "src/KeyPoll.h",
    "src/Labclass.h",
    "src/Logic.h",
    "src/MakeAndPlay.h",
    "src/Map.h",
    "src/Maths.h",
    "src/Music.h",
    "src/Network.h",
    "src/Otherlevel.h",
    "src/physfsrwops.h",
    "src/preloader.h",
    "src/RenderFixed.h",
    "src/Render.h",
    "src/Screen.h",
    "src/ScreenSettings.h",
    "src/Script.h",
    "src/SoundSystem.h",
    "src/Spacestation2.h",
    "src/Textbox.h",
    "src/TowerBG.h",
    "src/Tower.h",
    "src/Unused.h",
    "src/UtilityClass.h",
    "src/Version.h",
    "src/Vlogging.h",
    "src/WarpClass.h",
    "src/XMLUtils.h",
    "src/main.h",
];

fn main() {
    let sdl2 = pkg_config::Config::new()
        .cargo_metadata(false)
        .probe("sdl2")
        .unwrap();

    let sdl2_mixer = pkg_config::Config::new()
        .cargo_metadata(false)
        .probe("SDL2_mixer")
        .unwrap();

    let mut cfg = cxx_build::bridge("src/main.rs");

    cfg.flag_if_supported("-Wno-format-security")
        .flag("-U_FORTIFY_SOURCE")
        .include("src")
        .includes(sdl2.include_paths)
        .includes(sdl2_mixer.include_paths);

    for file in FILES {
        cfg.file(file);
    }

    for file in FILES.iter().chain(HEADERS) {
        println!("cargo:rerun-if-changed={}", file);
    }

    println!("cargo:rerun-if-changed=src/main.rs");

    for (k, v) in sdl2.defines.into_iter().chain(sdl2_mixer.defines) {
        cfg.define(&k, v.as_deref());
    }

    if let Some(include) = std::env::var_os("DEP_PHYSFS_INCLUDE") {
        cfg.include(include);
    }

    cfg.compile("vvvvvv");

    pkg_config::probe_library("sdl2").unwrap();
    pkg_config::probe_library("SDL2_mixer").unwrap();

    println!("cargo:rustc-link-lib=physfs");
    println!("cargo:rustc-link-lib=lodepng");
    println!("cargo:rustc-link-lib=tinyxml2");
}
