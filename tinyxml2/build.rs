use cxx_build::CFG;
use std::env;
use std::path::Path;

fn main() {
    let manifest_dir = env::var_os("CARGO_MANIFEST_DIR").unwrap();
    let headers = Path::new(&manifest_dir).join("include");
    CFG.exported_header_dirs.push(&headers);

    cxx_build::bridge("src/lib.rs")
        .file("src/tinyxml2.cpp")
        .flag("-U_FORTIFY_SOURCE")
        .compile("tinyxml2");

    println!("cargo:rerun-if-changed=src/lib.rs");
    println!("cargo:rerun-if-changed=src/tinyxml2.cpp");
    println!("cargo:rerun-if-changed=include/tinyxml2.h");
}
