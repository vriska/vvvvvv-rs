use cxx_build::CFG;
use std::env;
use std::path::Path;

fn main() {
    let manifest_dir = env::var_os("CARGO_MANIFEST_DIR").unwrap();
    let headers = Path::new(&manifest_dir).join("include");
    CFG.exported_header_dirs.push(&headers);

    cxx_build::bridge("src/lib.rs")
        .file("src/lodepng.cpp")
        .define("LODEPNG_NO_COMPILE_ALLOCATORS", "1")
        .define("LODEPNG_NO_COMPILE_DISK", "1")
        .define("LODEPNG_NO_COMPILE_ENCODER", "1")
        .flag("-U_FORTIFY_SOURCE")
        .compile("lodepng");

    println!("cargo:rerun-if-changed=src/lib.rs");
    println!("cargo:rerun-if-changed=src/lodepng.cpp");
    println!("cargo:rerun-if-changed=include/lodepng.h");
}
