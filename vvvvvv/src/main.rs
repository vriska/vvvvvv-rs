use once_cell::sync::Lazy;
use rand::prelude::*;
use rand_xoshiro::Xoshiro128Plus;
use std::env;
use std::ffi::CString;
use std::os::raw::c_char;
use std::os::unix::ffi::OsStrExt;
use std::process::exit;
use std::ptr;
use std::sync::Mutex;

#[cxx::bridge]
mod ffi {
    extern "Rust" {
        fn xoshiro_seed(seed: u32);
        fn xoshiro_next() -> u32;
        fn xoshiro_rand() -> f32;
    }

    extern "C++" {
        include!("vvvvvv/src/main.h");

        unsafe fn cpp_main(argc: i32, argv: *mut *mut c_char) -> i32;
    }
}

static RNG: Lazy<Mutex<Option<Xoshiro128Plus>>> = Lazy::new(Default::default);

fn xoshiro_seed(seed: u32) {
    *RNG.lock().unwrap() = Some(Xoshiro128Plus::seed_from_u64(seed as u64));
}

fn xoshiro_next() -> u32 {
    RNG.lock().unwrap().as_mut().unwrap().gen()
}

fn xoshiro_rand() -> f32 {
    RNG.lock().unwrap().as_mut().unwrap().gen()
}

fn main() {
    let args: Vec<CString> = env::args_os()
        .map(|os_str| {
            let bytes = os_str.as_bytes();
            CString::new(bytes).unwrap_or_else(|nul_error| {
                let nul_position = nul_error.nul_position();
                let mut bytes = nul_error.into_vec();
                bytes.truncate(nul_position);
                CString::new(bytes).unwrap()
            })
        })
        .collect();

    let argc = args.len();
    let mut argv: Vec<*mut c_char> = Vec::with_capacity(argc + 1);
    for arg in &args {
        argv.push(arg.as_ptr() as *mut c_char);
    }
    argv.push(ptr::null_mut()); // Nul terminator.

    let ret = unsafe { ffi::cpp_main(argc as i32, argv.as_mut_ptr()) };

    exit(ret);
}
